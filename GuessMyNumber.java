import java.util.Scanner;

public class GuessMyNumber {
   public static void main(String[] args){
      int n;
      Scanner input = new Scanner(System.in);
      System.out.print("Enter n: ");
      n = input.nextInt();
       //validation of n
      while(n<=0){
         System.out.print("Enter a positive integer for n : ");
         n = input.nextInt();  
      }
      System.out.print("Welcome to Guess My Number!");
      System.out.println("Please think of number between 0 and "+ (n-1));
      approach(0, (n - 1), n);
      input.close();
   }
   //i is low an j is high, this function is required divide and conquer approach
   public static void approach(int i , int j,int n)
   {
      Scanner input = new Scanner(System.in);
      char response;
      if(i==j){ //every recursive DAC approach must have termination condition
    
         System.out.println("Is your number: "+i+"?");
         System.out.println("Please enter C for correct, H for too high, or L for too low.");
         System.out.print("Enter your response(H/L/C): ");
         response = input.next().charAt(0);
           //validation of character entered by user
         while((response!='C')&&(response!='H')&&(response!='L'))
         {
            System.out.print("Enter your response(H/L/C): ");
            response = input.next().charAt(0);
         }
         if(response=='C')
            System.out.println("Thanks you playing Guess my Number!");
         else
            System.out.println("ERROR! The number you have thought is not in range of 0 to "+(n-1));   
      }
      else
      {
      //here (0+31)/2 = 15.5 , so , ceil(15.5) = 16
         int m =(int) Math.ceil(((double)(i+j))/2.0);  
         System.out.println("Is your number: "+m+"?");
         System.out.println("Please enter C for correct, H for too high, or L for too low.");
         System.out.print("Enter your response(H/L/C): ");
         response = input.next().charAt(0);
           //validation of character entered by user
         while((response!='C')&&(response!='H')&&(response!='L'))
         {
            System.out.print("Enter your response(H/L/C): ");
            response = input.next().charAt(0);
         }
         if(response=='C')
            System.out.println("Thanks you playing Guess my Number!");
         else if(response=='H')//means too high then we have to go in lower side
            approach(i,m-1,n);
         else
           approach(m + 1, j, n);
           input.close();
      }
    }
  }